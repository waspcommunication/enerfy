<div class="dn-head">
	<div class="row">
		<div class="small-12 columns">
			<div class="dn-head-top">
				<div>
					<a href="http://dn.se/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/dn-logo.jpg" /></a>
					<a href="#" class="toggle-dn-menu">
						<div>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
				</div>
				<div class="dn-border"></div>
			</div>
			<div class="dn-head-bottom">
				<?php

					$args = array(
						'menu' => 'dn-menu',
						'echo' => false
					);

					echo wp_nav_menu( $args );

				?>
			</div>
		</div>
	</div>
</div>

<div class="sidebar-videos">
	<div class="mplay">
		<?php
			$currentID = $post->ID;
			$args = array(
				'post_type' => 'posts',
				'categories' => 'ar-video'
			); 
			$the_query = new WP_Query( $args );
		?>
		<div class="mplay-episodes">
			<span class="mplay-top"><span></span>Videos</span>
			<p class="mplay-desc">Lorem ipsum dorum tjorum tjorum.</p>
			<div>
				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<?php if(get_field('extern')) {  ?>
					<a href="<?php the_field('extern'); ?>" target="_blank" class="mplay-episode">
				<?php } else { ?>
					<a href="<?php the_permalink(); ?>" class="mplay-episode">
				<?php } ?>
					<div class="img-wrapper">
					<?php if(has_post_thumbnail()) {
						the_post_thumbnail();
					} else {
						echo '<img src="'.get_bloginfo("template_url").'/assets/img/placeholder.jpg" />';
					} ?>
					</div>
					<div class="mplay-episode-info">
						<p><?php the_title(); ?></p>
						<h4><?php the_field('video_title'); ?></h4>
					</div>
				</a>
				<?php endwhile; endif; wp_reset_query(); ?>
			</div>
		</div>
	</div>
	<a href="#" class="to-videos">Se alla filmer</a>
</div>
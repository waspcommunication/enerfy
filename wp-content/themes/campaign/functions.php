<?php

	// enable custom menus
	add_theme_support( 'menus' );

	add_filter( 'show_admin_bar', '__return_false' );

	function any_ptype_on_cat($request) {
	if ( isset($request['category_name']) )
		$request['post_type'] = 'any';

	return $request;
	}
	add_filter('request', 'any_ptype_on_cat');

	// Load theme CSS
	function theme_styles() {

		wp_enqueue_style( 'main', get_template_directory_uri() . '/build/app.min.css', array(), '', 'screen' );
		
	}

	// Load theme JS
	function theme_js() {

		wp_enqueue_script( 'production', get_template_directory_uri() . '/build/app.min.js', array('jquery'), '', true );
	
	}

	add_action( 'wp_enqueue_scripts', 'theme_js' );

	add_action( 'wp_enqueue_scripts' , 'theme_styles' );

	function arphabet_widgets_init() {

		register_sidebar( array(
			'name' => 'contact-form',
		) );

	}

	add_action( 'widgets_init', 'arphabet_widgets_init' );

	add_theme_support( 'post-thumbnails' );
	add_image_size( 'mycustomsize', 600, 360, true );

	function custom_excerpt_length( $length ) {
        return 20;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	function custom_youtube_settings($code){
	    if(strpos($code, 'youtu.be') !== false || strpos($code, 'youtube.com') !== false){
	        $return = preg_replace("@src=(['\"])?([^'\">\s]*)@", "src=$1$2&showinfo=0&rel=0&autohide=1", $code);
	        return $return;
	    }
	    return $code;
	}
	 
	add_filter('embed_handler_html', 'custom_youtube_settings');
	add_filter('embed_oembed_html', 'custom_youtube_settings');

	function wp_editor_fontsize_filter( $buttons ) {
	        array_shift( $buttons );
	        array_unshift( $buttons, 'fontsizeselect');
	        array_unshift( $buttons, 'formatselect');
	        return $buttons;
	}    
	add_filter('mce_buttons_2', 'wp_editor_fontsize_filter');

	if ( !function_exists( 'wp_tinymce_font_sizes' ) ) {
		function wp_tinymce_font_sizes( $sizes ){
			$sizes['fontsize_formats'] = "9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 26px 28px 30px 32px 34px 36px 40px 45px 46px 52px 56px 62px";
			return $sizes;
		}
	}
	add_filter( 'tiny_mce_before_init', 'wp_tinymce_font_sizes' );

	function demo($mimes) {
		if ( function_exists( 'current_user_can' ) )
			$unfiltered = $user ? user_can( $user, 'unfiltered_html' ) : current_user_can( 'unfiltered_html' );
		if ( !empty( $unfiltered ) ) {
			$mimes['swf'] = 'application/x-shockwave-flash';
		}
		return $mimes;
	}
	add_filter('upload_mimes','demo');

	function my_theme_add_editor_styles() {
	    add_editor_style( 'editor-style.css' );
	}
	add_action( 'admin_init', 'my_theme_add_editor_styles' );

	// Callback function to insert 'styleselect' into the $buttons array
	function my_mce_buttons_2( $buttons ) {
	    array_unshift( $buttons, 'styleselect' );
	    return $buttons;
	}
	// Register our callback to the appropriate filter
	add_filter('mce_buttons_2', 'my_mce_buttons_2');

	// Callback function to filter the MCE settings
	function my_mce_before_init_insert_formats( $init_array ) {  
	    // Define the style_formats array
	    $style_formats = array(  
	        // Each array child is a format with it's own settings
	        array(  
	            'title' => 'Show only mobile',  
	            'selector' => 'a',  
	            'classes' => 'link-mobile'         
	        ),
	        array(  
	            'title' => 'Show only tablet',  
	            'selector' => 'a',  
	            'classes' => 'link-tablet'         
	        ),
	        array(  
	            'title' => 'Show only desktop',  
	            'selector' => 'a',  
	            'classes' => 'link-desktop'         
	        ),
	        array(  
	            'title' => 'Show only sporttidningen',  
	            'selector' => 'a', 
	            'classes' => 'show-betser'         
	        ),
	        array(  
	            'title' => 'Show only expressen', 
	            'selector' => 'a', 
	            'classes' => 'show-expressen'         
	        ),
	    );  
	    // Insert the array, JSON ENCODED, into 'style_formats'
	    $init_array['style_formats'] = json_encode( $style_formats );  

	    return $init_array;  

	} 
	// Attach callback to 'tiny_mce_before_init' 
	add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

?>

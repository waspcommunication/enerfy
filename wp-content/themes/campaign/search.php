<?

// Template name: Search result

get_header(); ?>

		<div class="row main-content search-results">
			<div class="medium-8 columns end border-right-gray">

				 <?php if (have_posts()) : ?>

					 <h2><?php printf( __( 'Sökresultat för: %s', 'campaign' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
				    
				    <?php while (have_posts()) : the_post(); ?>

				    <a href="<?php the_permalink() ?>">
					  	<h2><?php the_title(); ?></h2>
					  	<p><?php the_excerpt(); ?></p>
					</a>

				<?php endwhile; ?> 

				<?php else : ?> 

				<?php _e( 'Inga sökresultat hittades' ); ?> 

				<?php endif; ?>
			
			</div>
			<aside class="medium-4 columns end">
				<div class="inner">
					<?php get_search_form(); ?>
				</div>
			</aside>
		</div>

<?php get_footer(); ?>
<aside class="medium-4 columns end">
	<div class="inner">
		<?php get_search_form(); ?>
	</div>
    <div class="popular">
        <h3 class="sidebar-article-title border-bottom-blue">Mest läst</h3>
        <div class="popular-posts popular-articles">
        <?php
            $args = array(
                'post_type' => array('sidebar-posts', 'posts'),
                'posts_per_page' => 5,
                'meta_key' => 'views',
                'orderby' => 'meta_value_num',
                'order' => 'DESC'
            );
            $the_query = new WP_Query( $args );

        if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <?php $popThumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

            <a href="<?php the_permalink(); ?>" class="popular-post">
                <figure class="popular-thumbnail bg-img" style="background-image: url(<?php echo $popThumb[0]; ?>);"></figure>
                <p class="popular-post-title">
                	<?php if(get_field('heading')) { the_field('heading'); } else { the_title(); } ?></p>
                <span class="btn-more">Läs mer</span>
            </a>

        <?php endwhile; endif; wp_reset_query(); ?>
   		</div>
	</div>
	<?php
		$currentID = $post->ID;
		$args = array(


			'post_type' => 'sidebar-posts',
			'post__not_in' => array($currentID),
			'orderby' => 'menu_order',
			'order' => 'ASC'

		); 
		$the_query = new WP_Query( $args );
	?>
	<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	
	<div class="inner">
		<?php if(get_field('annons-img')) { ?>

			<a href="<?php the_field('extern'); ?>" target="_blank">
				<img src="<?php the_field('annons-img'); ?>" />
			</a>

		<?php } else if(get_field('flash')) { ?>

			<div class="flash-wrapper">
				<?php if(get_field('extern')) { ?>
				<a class="flash-url" href="<?php the_field('extern'); ?>"></a>
				<?php } ?>
				<?php the_field('flash'); ?>
			</div>

		<?php } else { ?>
		<h4 class="border-bottom-blue"><?php the_title(); ?></h4>
		<?php if(get_field('video')){ 
			echo "<div class='img-wrapper'>" . get_field('video') . "</div>";
		} else if(get_field('bildspel')){ 
			the_field('bildspel');
		} else if(has_post_thumbnail()) { ?>
			<?php if(get_field('extern')){ ?>
			<a href="<?php the_field('extern'); ?>" target="_blank">
			<?php } else { ?>
			<a href="<?php the_permalink(); ?>">
			<?php } ?>
				<div class="img-wrapper">
				   <?php the_post_thumbnail('mycustomsize'); ?>
				</div>
			</a>
		<?php } ?>
		<?php if(get_field('extern')){ ?>
		<a href="<?php the_field('extern'); ?>" target="_blank">
		<?php } else { ?>
		<a href="<?php the_permalink(); ?>">
		<?php } ?>
			<h3><?php if(get_field('heading')){ the_field('heading'); } ?></h3>
			<div><?php  if(get_field('short-info')){ the_field('short-info'); } ?></div>
		</a>
		<?php } ?>
	</div>

	<?php endwhile; endif; wp_reset_query(); ?>

	<?php get_template_part('templates/sidebar', 'videos'); ?>

	<?php
		$args = array(
			'post_type' => 'facebook-link',
			'posts-per-page' => 1
		); 
		$the_query = new WP_Query( $args );
	?>
	<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	
	<?php if(get_field('facebook-url')){ ?>
	<div class="likebox-wrapper">
		<div class="fb-page" 
		  data-href="<?php the_field('facebook-url'); ?>"
		  data-tabs="timeline"
		  data-width="380" 
		  data-hide-cover="false"
		  data-show-facepile="false" 
		  data-show-posts="false"></div>
	</div>
	<?php } ?>
	<?php endwhile; endif; wp_reset_query(); ?>
</aside>
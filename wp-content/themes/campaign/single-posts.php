<?php

// Template name: Home

get_header(); ?>

	<main>

		<div class="row main-content">
			<div class="medium-8 columns end posts border-right-gray single-post">
				<div class="row">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article class="large-12 columns">
						<div class="inner">
							<div class="row post-area">
								<div class="large-12 columns end post-top">
								<?php
									if(get_field('video')){ 
										echo "<div class='img-wrapper'>" . get_field('video') . "</div>";
									} else if(has_post_thumbnail()) {
									    the_post_thumbnail();
									} else {
										echo '<img src="'.get_bloginfo("template_url").'/assets/img/placeholder.jpg" />';
									}
								?>
								</div>
								<div class="<?php if(get_field('faktaruta')){ echo "large-9"; } else { echo "large-12"; } ?> columns end">
									<h2><?php if(get_field('rubrik')){ the_field('rubrik'); } else { the_title(); } ?></h2>
									<p class="ingress"><?php the_field('ingress'); ?></p>
									<?php if(get_field('introlank')) { ?>
										<a class="intro-link" href="<?php the_field('introlank_url'); ?>" target="_blank"><?php the_field('introlank'); ?></a>
									<?php } ?>
									<div class="main-post-content"><?php the_field('main-content'); ?></div>
								</div>
								<?php if(get_field('faktaruta')){
										echo "<div class='large-3 columns end'><div class='facts'>" . get_field('faktaruta') . "</div></div>";
								} ?>
								<?php if( have_rows('products') ): ?>
								<div class="large-12 columns">
									<div class="product-board">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/kundvagn.png" /><p>Köp produkterna här</p>
									</div>
									<div class="flexslider product-slider">
										<ul class="slides">

										<?php while( have_rows('products') ): the_row(); ?>

											<?php if( have_rows('slide') ): ?>

												<li>
													<div class="slide-inner">
														<?php while( have_rows('slide') ): the_row(); ?>

														<div class="product-wrapper">
															<div class="img-wrapper">
																<img src="<?php the_sub_field('img') ?>" />
															</div>
															<h4><?php the_sub_field('title'); ?></h4>
															<p><?php the_sub_field('desc'); ?></p>
															<a href="<?php the_sub_field('url'); ?>" target="_blank">Köp nu</a>
														</div>

														<?php endwhile; ?>
													</div>

												</li>

											<?php endif; ?>

										<?php endwhile; ?>

										</ul>
									</div>
								</div>
								<?php endif; ?>
								<div class="large-12 columns">
						 			<div class="social-share__inner">
										<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="js-social-share__link social-share__link social-share__link--facebook fa fa-facebook">Dela på Facebook</a>
										<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" class="js-social-share__link social-share__link social-share__link--twitter fa fa-twitter"></a>
									</div>
								</div>
							</div>
						</div>
					</article>
					<?php endwhile; endif; wp_reset_query(); ?>					
				</div>
				<?php get_template_part('moreposts'); ?>
			<?php get_sidebar(); ?>
		</div>

	</main>			

<?php get_footer(); ?>
<!DOCTYPE html>

	<head>

		<title><?php wp_title(); ?></title>

		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

		<?php if(is_single()) { ?>

			<?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>

			<meta property="og:title" content="Annons: <?php the_title(); ?>" />
			<meta property="og:type" content="article" />
			<meta property="og:url" content="<?php the_permalink(); ?>" />
			<meta property="og:image" content="<?php echo $thumbnail_src[0]; ?>" />

			<meta name="twitter:card" content="summary">
			<meta name="twitter:url" content="<?php the_permalink(); ?>">
			<meta name="twitter:title" content="Annons: <?php the_title(); ?>">
			<meta name="twitter:image" content="<?php echo $thumbnail_src[0]; ?>">

		<?php } ?>

		<?php wp_head()?>

		<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

		<script type="text/javascript" src="http://ll.lp4.io/app/53/5a/7c/535a7c5fe45a1d4e46d8d7b5.js?d=20141127"></script>

		<?php if(get_field('head_area', 5)) {
			the_field('head_area', 5);
		} ?>

	</head>

	<body>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/sv_SE/sdk.js#xfbml=1&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<?php if(get_field('global_pixel', 5)) { ?>
			<div class="pixel" style="overflow: hidden; height: 0 !important; width: 0 !important;">
				<?php the_field('globa_pixel', 5); ?>
			</div>
		<?php } ?>

		<?php if(get_field('article_pixel')) { ?>
			<div class="pixel" style="overflow: hidden; height: 0 !important; width: 0 !important;">
				<?php the_field('article_pixel'); ?>
			</div>
		<?php } ?>

		<?php
			if(get_field('header', 5)) {
				$header = get_field('header', 5) ?>

				<section class="site-head site-head--<?php echo $header; ?>">
					<div class="row">
						<div class="small-12 columns">
							<div class="site-head__top">
								<a href="http://expressen.se/<?php echo $header; ?>" target="_blank" class="site-head__logo-desktop"></a>
								<a href="http://expressen.se" target="_blank" class="site-head__expressen">Expressen.se</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<nav class="site-head__inner">
								<button type="button" class="toggle-mobile-nav"></button>
								<a href="http://expressen.se/<?php echo $header; ?>" target="_blank" class="site-head__logo-mobile"></a>
								<div class="site-head__menu">
									<?php

										$args = array(
											'menu' => $header,
											'echo' => false
										);

										echo wp_nav_menu( $args );

									?>
								</div>
								<a href="https://id.expressen.se/?return_url=http://www.expressen.se/<?php echo $header; ?>" class="fa fa-user site-head__login">Logga in</a>
							</nav>
						</div>
					</div>
				</section>

				<div class="site-head-label">
					<div class="site-head-label__inner js-site-label">
						<div class="row">
							<div class="small-12 columns">
								<div class="site-label">hela denna avdelning är en annons från <?php echo get_bloginfo( 'name' ); ?></div>
							</div>
						</div>
					</div>
				</div>

			<?php } else { ?>

				<header class="site-header js-site-header">
					<a href="http://expressen.se/" class="site-header__inner" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/expressenlogo.svg');" target="_blank"></a>
					<div class="site-label">hela denna avdelning är en annons från <?php echo get_bloginfo( 'name' ); ?></div>
					<div class="site-header__enerfy">Tips och inspiration från Enerfy</div>
				</header>
				<div class="site-head-fill"></div>

			<?php }
		?>

		<section class="banner">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$args = array(

							'post_type' => 'banner'

						);
						$the_query = new WP_Query( $args );
					?>
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<?php if(get_field('banner_ad')) { ?>
							<div class="banner_ad">
								<?php the_field('banner_ad'); ?>
							</div>
						<?php } else { ?>
		 					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<img src="<?php the_field('banner-img'); ?>" />
							</a>
						<?php } ?>

					<?php endwhile; endif; wp_reset_query(); ?>
				</div>
			</div>
		</section>

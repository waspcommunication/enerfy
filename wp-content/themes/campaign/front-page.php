<?php

// Template name: Home

get_header(); ?>

	<main>

		<div class="row main-content">
			<div class="medium-8 columns end posts border-right-gray">
				<?php get_template_part( 'posts' ); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>

	</main>			

<?php get_footer(); ?>
				<div class="row more-posts">
					<?php if(get_field('relation')){ ?>
					<div class="large-12 columns">
						<h4 class="border-bottom-gray">Läs mer</h4>
					</div>
					<?php
						$ids = get_field('relation', false, false);
						$args = array(

						'post_type' => array('posts', 'sidebar-posts'),
						'posts_per_page' => 3,
						'post__in' => $ids

						);
						$the_query = new WP_Query( $args );	
						if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
					<article class="medium-4 columns end">
						<div class="img-wrapper">
							
						<?php if(has_post_thumbnail()) {
						    the_post_thumbnail('mycustomsize');
							} else if(get_field('video')){ 
								the_field('video');
							} else {
								echo '<img src="'.get_bloginfo("template_url").'/assets/img/placeholder.jpg" />';
							}
						?>
						</div>
						<a href="<?php if(get_field('extern')){ the_field('extern'); } else { the_permalink(); } ?>">
						<h3><?php if(get_field('related-rubrik')){ the_field('related-rubrik'); } else { the_title(); } ?></h3>
							
							<?php if(get_field('related-short')){ the_field('related-short'); } else { the_field('short-info'); } ?>
							
						</a>
					</article>
					<?php endwhile; endif; wp_reset_query(); } ?>
					<?php if(get_field('relationnoimg')){ ?>
					<div class="large-12 columns">
						<h4 class="border-bottom-gray">Läs mer</h4>
					</div>
					<?php
						$ids = get_field('relationnoimg', false, false);
						$args = array(

						'post_type' => array('posts', 'sidebar-posts'),
						'post__in' => $ids

						);
						$the_query = new WP_Query( $args );
						if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
					?>
					<article class="large-12 columns end older-post">
						<a href="<?php if(get_field('extern')){ the_field('extern'); } else { the_permalink(); } ?>" class="short-info"><?php if(get_field('related-short')){ the_field('related-short'); } else { the_field('short-info'); } ?></a>
					</article>
					<?php endwhile; endif; wp_reset_query(); } ?>
				</div>
				<div class="big-border"></div>
				<?php get_template_part( 'posts' ); ?>
			</div>
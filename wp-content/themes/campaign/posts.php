				<div class="row">
					<?php
						$args = array(

							'post_type' => 'posts',
							'orderby' => 'menu_order',
							'order' => 'ASC',
							'tax_query' => array(
							    array(
							        'taxonomy'  => 'categories',
							        'field'     => 'slug',
							        'terms'     => 'visa-ej-i-artikelflode', // exclude items media items in the news-cat custom taxonomy
							        'operator'  => 'NOT IN'
							   ),
							 )
						); 
						$the_query = new WP_Query( $args );
					?>
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	
					<article class="<?php the_field('width'); ?> columns">
						<?php if(get_field('annons-img')) { ?>

							<div class="inner add-inner">
								<a href="<?php the_field('extern'); ?>" target="_blank">
									<img src="<?php the_field('annons-img'); ?>" />
								</a>
							</div>

						<?php } else if(get_field('flash')) { ?>

							<div class="flash-wrapper">
								<?php if(get_field('extern')) { ?>
								<a class="flash-url" href="<?php the_field('extern'); ?>"></a>
								<?php } ?>
								<?php the_field('flash'); ?>
							</div>

						<?php } else { ?>
						<div class="inner border-bottom-gray">
							<a href="<?php if(get_field('extern')){ echo the_field('extern'); } else { the_permalink(); } ?>">
								<div class="dotted-right"></div>
								<div class="img-wrapper">
									<?php
										if(has_post_thumbnail()) {
										    the_post_thumbnail();
										} else if(get_field('video')){ 
											the_field('video');
										} else {
											echo '<img src="'.get_bloginfo("template_url").'/assets/img/placeholder.jpg" />';
										}
									?>
								</div>
								<h2><?php if(get_field('rubrik')){ the_field('rubrik'); } else { the_title(); } ?></h2>
								<div class="short-info"><?php the_field('short-info'); ?></div>
							</a>
						</div>
						<?php } ?>
					</article>
					<?php endwhile; endif; wp_reset_query(); ?>
				</div>
export default class SiteHeader {

	constructor() {
		this.header = document.querySelector('.js-site-header')
		this.isScrolled = false
		this.scroll = 0

		this.addListener()
	}

	onScroll() {
		this.scroll = window.scrollY

		if(!this.isScrolled && this.scroll >= 300) {
			this.header.classList.add('is-scrolled')

			this.isScrolled = true
		} else if(this.isScrolled && this.scroll <= 300) {
			this.header.classList.remove('is-scrolled')

			this.isScrolled = false
		}
	}

	addListener() {
		window.addEventListener('scroll', () => {
			this.onScroll()
		})
	}
}

import TweenMax from 'gsap'

export default class ReadMore {

  constructor() {
    this.readmore = document.querySelector('.js-rec')
    this.close = this.readmore.querySelector('.js-rec__close')

    this.scrollPos = 0
    this.isScrolled = false

    this.tl = new TimelineLite({ paused: true })

    this.tl
      .set(this.readmore, { autoAlpha: 1 })
      .from(this.readmore, 0.5, { yPercent: 100, ease: Expo.easeInOut })

    this.addListeners()
  }

  show() {
    this.tl.play()
  }

  hide() {
    this.tl.reverse()
  }

  onScroll() {
    this.scrollPos = window.scrollY

    if(!this.isScrolled && this.scrollPos >= 500) {
      this.show()

      this.isScrolled = true
    } else if(this.isScrolled && this.scrollPos < 500) {
      this.hide()

      this.isScrolled = false
    }
  }

  addListeners() {
    window.addEventListener('scroll', () => {
      this.onScroll()
    })

    this.close.addEventListener('click', this.hide.bind(this))
  }
}

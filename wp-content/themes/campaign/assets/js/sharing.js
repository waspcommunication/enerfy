export default class Sharer {

	constructor(){
		this.links = [...document.querySelectorAll('.js-social-share__link')]

		this.init()
	}

	openUrl(){
		this.links.forEach((el) => {
			const url = el.getAttribute('href');

			el.addEventListener('click', (el) => {
				el.preventDefault()
				window.open(url, 'sharer', 'toolbar=0,status=0,width=648,height=395')
			})
		})

	}

	init(){
		this.openUrl()
	}

}
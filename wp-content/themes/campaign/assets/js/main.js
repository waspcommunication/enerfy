import SiteHeader from './header'
import Sharer from './sharing'
import ReadMore from './readmore'

const sharer = new Sharer();
const siteHeader = new SiteHeader()

if(document.querySelector('.js-rec')) {
  const readMore = new ReadMore()
}

(function($) {

  function toggleDn(){
    $('.toggle-dn-menu').click(function(e){
      e.preventDefault();
      $('.dn-head-bottom').slideToggle(150);
    });
  }

  toggleDn();

  // Look at all h2's
  var $withSpans = $('h2 p').filter(function(){
      // If it has at least one span, save it
      if(!$('span', $(this)).length)
          return true
      // Otherwise remove it from the selector
      else
          return false
  })
  // Finally, add the class
  $withSpans.addClass('font-size')

  $('.question-wrapper').click(function(e){
    e.preventDefault();
    $('.answer-wrapper', this).slideToggle(100);
    $(this).toggleClass('active');
    $(this).siblings().find('.answer-wrapper').slideUp(100);
    $(this).siblings().removeClass('active');
  });
  $('.toggle-all-questions').click(function(e){
    e.preventDefault();
    $('.answer-wrapper').slideDown(100);
    $('.question-wrapper').addClass('active');
  });

  $('.js-secondary-nav__toggle').click(function(){
    $('.js-secondary-nav__menu').slideToggle(150);
  });

  function equalheight(container){
    var currentTallest = 0,
         currentRowStart = 0,
         rowDivs = new Array(),
         $el,
         topPosition = 0;
     $(container).each(function() {

       $el = $(this);
       $($el).height('auto')
       topPostion = $el.position().top;

       if (currentRowStart != topPostion) {
         for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
           rowDivs[currentDiv].height(currentTallest);
         }
         rowDivs.length = 0; // empty the array
         currentRowStart = topPostion;
         currentTallest = $el.height();
         rowDivs.push($el);
       } else {
         rowDivs.push($el);
         currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
       for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
         rowDivs[currentDiv].height(currentTallest);
       }
     });
  }

  $(window).load(function() {
    equalheight('.posts .medium-6');
  });

  $(window).resize(function(){
    equalheight('.posts .medium-6');
  });

  $('.toggle-dina-pengar-menu').click(function(event){
    event.preventDefault();
    if($('.dina-pengar-menu').hasClass('active')){
        $('.dina-pengar-menu').removeClass('active');
    } else {
        $('.dina-pengar-menu').addClass('active');
    }
  });

  function newMobile(){

      var btn = $('.toggle-mobile-nav'),
          nav = $('.site-head__menu');

      btn.click(function(){
        nav.slideToggle(150);
      });

  }

  newMobile();

})( jQuery );

<?php if(is_single() && get_field('extern_eller_intern')) { ?>

	<article class="rec js-rec">
		<div class="row">
			<div class="small-12 column">
				<div class="rec__inner">
					<a href="<?php the_field('readmore_extern_url'); ?>" class="rec__link">
						<div class="rec__content">
							<h1 class="rec__title"><?php the_field('readmore_extern_title'); ?></h1>
							<p class="rect__desc"><?php the_field('readmore_extern_desc'); ?></p>
						</div>
						<figure class="rec__thumb">
							<img src="<?php the_field('readmore_extern_img'); ?>">
						</figure>
					</a>
					<button type="button" class="rec__close js-rec__close fa fa-close"></button>
				</div>
			</div>
		</div>
	</article>

<?php } else if(is_single() && get_field('readmore_link')) {

	$post_object = get_field('readmore_link');

	if( $post_object ):

	$post = $post_object;
	setup_postdata( $post );

	?>
		<article class="rec js-rec">
			<div class="row">
				<div class="small-12 column">
					<div class="rec__inner">
						<a href="<?php the_permalink(); ?>" class="rec__link">
							<div class="rec__content">
								<h1 class="rec__title"><?php the_field('readmore_title'); ?></h1>
								<p class="rect__desc"><?php the_field('readmore_desc'); ?></p>
							</div>
							<figure class="rec__thumb">
								<?php the_post_thumbnail('medium'); ?>
							</figure>
						</a>
						<button type="button" class="rec__close js-rec__close fa fa-close"></button>
					</div>
				</div>
			</div>
		</article>
	<?php wp_reset_postdata(); endif;
} ?>

	<?php if(!is_404()) { ?>
		<footer>
			<div class="row footer-top">
				<div class="large-12 columns">
					<?php
						$args = array(

							'post_type' => 'footer-info',
							'posts-per-page' => 1

						);
						$the_query = new WP_Query( $args );
					?>
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<img src="<?php the_field('logo'); ?>" /><p><?php the_field('footer-text'); ?></p>
				</div>
			</div>
			<div class="row footer-bottom">
				<div class="large-12 columns">
					<p><?php the_field('footer-copyright'); ?></p>
					<?php endwhile; endif; wp_reset_query(); ?>
					<ul>
						<?php
							$args = array(

								'post_type' => 'footer-social'

							);
							$the_query = new WP_Query( $args );
						?>
						<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
						if(get_field('link_to_social_account')){
							echo '<li><a href="' . get_field('link_to_social_account') . '"><div class="tool-tip">' . get_the_title() . '</div><i class="icon-' . get_the_title() . '"></i></a></li>';
						} endwhile; endif; wp_reset_query(); ?>
					</ul>
				</div>
			</div>
		</footer>
		<div class="publisher">
			<div class="row">
				<div class="medium-6 columns large-centered">
					<div>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/geting.svg" />
						<p>AB Kvällstidningen Expressen<br />Ansvarig utgivare: Thomas Mattsson<br/>Produktion:<a href="http://waspcommunication.se/" target="_blank">Wasp Communication</a><br/><a href="http://www.expressen.se/om-expressen/om-cookies-pa-expressense/" target="_blank">Om cookies</a>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

		<?php wp_footer(); ?>

		<?php
		    global $post;
		    $post_slug=$post->post_name;
		?>

		<script>
		window.Bonnier = window.Bonnier || {};
		window.Bonnier.Expressen = window.Bonnier.Expressen || {};
		window.Bonnier.Expressen.Web = window.Bonnier.Expressen.Web || {};
		</script>

		<script>

			var isMobile = window.innerWidth < 768;

			if (isMobile) {
			  window.s_account = "exabmob";
			} else {
			  window.s_account = "exabprod";
			}

		</script>

		<script language="javascript" src="http://www.expressen.se/static/scripts/s_code.js" type="text/javascript"></script>

		<script language="javascript" type="text/javascript">
			(function(s){
			s.pageName="sitenamehere:<?= $post_slug; ?>";
			s.channel="annonssida";
			s.hier1="annonssida:sitenamehere";
			s.prop1="sitenamehere";
			s.prop4="article";
			s.prop5="<?= $post_slug; ?>";
			s.prop11="kampanj.expressen.se/sitenamehere";
			s.prop41="unlocked";

			/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
			var s_code=s.t();if(s_code)document.write(s_code)})(s);
		</script>

	    <script language="JavaScript" type="text/javascript"><!--
	    if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
	    //--></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img src="http://metrics.expressen.se/b/ss/" exabprod="/1/H.20.3--NS/0?gn=noscript-sitenamehere-<?= $post_slug; ?>" height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->
	    <!-- End SiteCatalyst code version: H.19.3. -->
		</script>

	</body>

</html>
